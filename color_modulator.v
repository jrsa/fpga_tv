`default_nettype none
`timescale 1ns/1ps

module color_modulator(
  input [4:0] subcarrier_counter,
  input [4:0] color,
  input sat,
  input video_active,
  input burst_active,

  output subcarrier
);

  assign subcarrier = (chrom & video_active & sat) | ((subcarrier_counter < 16) & burst_active);

  reg chrom;

  always @(*)
    begin
      case (color)
        5'b00000:
          chrom = (subcarrier_counter >= 0 && subcarrier_counter < 16);
        5'b00001:
          chrom = (subcarrier_counter >= 1 && subcarrier_counter < 17);
        5'b00010:
          chrom = (subcarrier_counter >= 2 && subcarrier_counter < 18);
        5'b00011:
          chrom = (subcarrier_counter >= 3 && subcarrier_counter < 19);
        5'b00100:
          chrom = (subcarrier_counter >= 4 && subcarrier_counter < 20);
        5'b00101:
          chrom = (subcarrier_counter >= 5 && subcarrier_counter < 21);
        5'b00110:
          chrom = (subcarrier_counter >= 6 && subcarrier_counter < 22);
        5'b00111:
          chrom = (subcarrier_counter >= 7 && subcarrier_counter < 23);
        5'b01000:
          chrom = (subcarrier_counter >= 8 && subcarrier_counter < 24);
        5'b01001:
          chrom = (subcarrier_counter >= 9 && subcarrier_counter < 25);
        5'b01010:
          chrom = (subcarrier_counter >= 10 && subcarrier_counter < 26);
        5'b01011:
          chrom = (subcarrier_counter >= 11 && subcarrier_counter < 27);
        5'b01100:
          chrom = (subcarrier_counter >= 12 && subcarrier_counter < 28);
        5'b01101:
          chrom = (subcarrier_counter >= 13 && subcarrier_counter < 29);
        5'b01110:
          chrom = (subcarrier_counter >= 14 && subcarrier_counter < 30);
        5'b01111:
          chrom = (subcarrier_counter >= 15 && subcarrier_counter < 31);
        5'b10000:
          chrom = (subcarrier_counter >= 16 && subcarrier_counter < 32);
        5'b10001:
          chrom = (subcarrier_counter >= 17 || subcarrier_counter < 1);
        5'b10010:
          chrom = (subcarrier_counter >= 18 || subcarrier_counter < 2);
        5'b10011:
          chrom = (subcarrier_counter >= 19 || subcarrier_counter < 3);
        5'b10100:
          chrom = (subcarrier_counter >= 20 || subcarrier_counter < 4);
        5'b10101:
          chrom = (subcarrier_counter >= 21 || subcarrier_counter < 5);
        5'b10110:
          chrom = (subcarrier_counter >= 22 || subcarrier_counter < 6);
        5'b10111:
          chrom = (subcarrier_counter >= 23 || subcarrier_counter < 7);
        5'b11000:
          chrom = (subcarrier_counter >= 24 || subcarrier_counter < 8);
        5'b11001:
          chrom = (subcarrier_counter >= 25 || subcarrier_counter < 9);
        5'b11010:
          chrom = (subcarrier_counter >= 26 || subcarrier_counter < 10);
        5'b11011:
          chrom = (subcarrier_counter >= 27 || subcarrier_counter < 11);
        5'b11100:
          chrom = (subcarrier_counter >= 28 || subcarrier_counter < 12);
        5'b11101:
          chrom = (subcarrier_counter >= 29 || subcarrier_counter < 13);
        5'b11110:
          chrom = (subcarrier_counter >= 30 || subcarrier_counter < 14);
        5'b11111:
          chrom = (subcarrier_counter >= 31 || subcarrier_counter < 15);
        // if this gets hit, there was a typo somewhere else in the case statement,
        // which is honestly not hard to believe ;)
        // default:
        //   chrom = (subcarrier_counter < 16);
      endcase
    end

endmodule

