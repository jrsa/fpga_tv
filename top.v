// vim: ts=2:sw=2

`default_nettype none


// only physical i/o signals should be going in and out of this module
// (see PCF file for their signal names)
module top (
//  input clk, // clk input from 12MHz oscillator to fgpa pin PIO3_00 (physical pin 21)

  // TODO: add a layer of indirection between these net names and the code, to ease porting to other boards 
  output P1A1,
  output P1A2,
  output P1A3,
  input  P1A4,
  input  BTN1,
  input  BTN2,
  input  BTN3,
  output  LED1,
  output  LED2,
  output  LED3,
  output  LED4,
  output  LED5,
);


  wire clk_i_4x_csc;
  wire clk_pll_32x_csc;

  reg [4:0] vid_clk;

  assign clk_i_4x_csc = P1A4;

  /*
  $ icepll -i 14.318 -o 114.544

  F_PLLIN:    14.318 MHz (given)
  F_PLLOUT:  114.544 MHz (requested)
  F_PLLOUT:  114.544 MHz (achieved)
  */
   SB_PLL40_CORE #(
		.FEEDBACK_PATH("SIMPLE"),
    .DIVR(4'b0000),
    .DIVF(7'b0111111),
    .DIVQ(3'b011),
    .FILTER_RANGE(3'b001)
	) pll_inst (
		.RESETB(1'b1),
		.BYPASS(1'b0),
		.REFERENCECLK(clk_i_4x_csc),
    .PLLOUTGLOBAL(clk_pll_32x_csc)
  );

  // sync output ------------------------------------------------------------------- 
  wire csync, hsync, vsync;
  assign csync = hsync ^ vsync;
  assign P1A1 = ~csync;
  //-------------------------------------------------------------------------------- 

  // divide master clock
  always @(posedge clk_pll_32x_csc)
    begin
      vid_clk <= vid_clk + 1;
    end
  

  wire [9:0] pixel_x;
  wire [9:0] pixel_y;
  wire video_on;
  
  // video output
  wire luma;
  assign P1A2 =  video_on & luma;

  // color subcarrier output
  wire burst;
  wire sat;

  wire subcarrier;
  assign P1A3 = subcarrier;

  wire [4:0] color;

  color_modulator color_mod (
    .subcarrier_counter(vid_clk),
    .color(color),
    .sat(sat),
    .burst_active(burst),
    .video_active(video_on),
    .subcarrier(subcarrier)
  );

/* verilator lint_off PINMISSING */
  vga_sync sync_gen (
    .clk(vid_clk[2]),
    .reset(1'b0),
    .hsync(hsync),
    .vsync(vsync),
    .pixel_x(pixel_x),
    .pixel_y(pixel_y),
    .video_on(video_on),
    .burst(burst)
  );
/* verilator lint_on PINMISSING */

  pattern pattern_inst (
    .pixel_x(pixel_x),
    .pixel_y(pixel_y),

    .color(color),
    .sat(sat),
    .luma(luma)
  );

endmodule
