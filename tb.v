`default_nettype none
`timescale 1ns/1ps

module tb;
  reg clkin; 
  reg reset;

  wire hsync, vsync, video_on, burst;
  wire [9:0] pixel_x, pixel_y;

  vga_sync vga_sync_inst (
    .clk(vid_clk[2]), // this is the goofy part, the sync generator wouldn't meet timing on ice40 using the 114MHz clk. 
                      // from nextpnr-ice40: "Warning: Max frequency for clock 'vid_clk[2]_$glb_clk': 68.53 MHz (FAIL at 114.54 MHz)"
    .reset(reset),

    .hsync(hsync),
    .vsync(vsync), 
    .video_on(video_on), 
    .burst(burst),

    .pixel_x(pixel_x),
    .pixel_y(pixel_y)

  );

  wire color;
  wire sat;
  wire luma;

  color_modulator color_mod_inst (
    .subcarrier_counter(vid_clk),
    .color(color),
    .sat(sat),
    .burst_active(burst),
    .video_active(video_on)
    // .subcarrier(subcarrier)  // this is the output
  );

  pattern pattern_inst (
    .pixel_x(pixel_x),
    .pixel_y(pixel_y),

    .color(color),
    .sat(sat),
    .luma(luma)
  );

    always
    begin
        // toggle clock every time slice
        // this constant corresponds to a 114.544MHz clock input
        #8.73 clkin <= ~clkin;
    end


    reg [4:0] vid_clk;
    always @(posedge clkin)
        begin
            if (reset)
                vid_clk <= 5'b00000;
            else
                vid_clk <= vid_clk + 1;
        end

    initial
    begin
    $dumpfile("dump.vcd");
    //$dumpvars(0, vga_sync_inst);
    $dumpvars(0, tb);

    clkin = 1'b0;
    reset = 1'b1;
    #1000
    reset = 1'b0;
    #10000000
    $finish;

    end

endmodule
