DESIGN_NAME = fpga_tv

# modules used in both sim and hw (ideally all of them except for a "sim toplevel" and a "hw toplevel
MODULES = vga_sync.v color_modulator.v pattern.v

SIM_TESTBENCH_TOP = tb.v

#--------------------------------------------------------------------------------
# icarus simulation
IV = iverilog
IV_OPTS = -Wall -g2012

SIM_MODULES = $(MODULES) $(SIM_TESTBENCH_TOP)
VVP_TARGET = $(DESIGN_NAME).vvp

# compile the verilog design and produce a vvp assembly file for simulation
# with the icarus `vvp` tool below
$(VVP_TARGET): $(SIM_MODULES)
	$(IV) -o $(VVP_TARGET) $(IV_OPTS) $(SIM_MODULES)

# produce a `vcd` (value change dump) file
# TODO: this file is named in the testbench verilog file, whatever i guess?
# would be sick to have it timestamped when the command was run
dump.vcd: $(VVP_TARGET)
	vvp $(VVP_TARGET)

.phony: wave clean_sim

wave: dump.vcd
	gtkwave -S wave.tcl dump.vcd

clean_sim:
	rm dump.vcd $(VVP_TARGET)

#--------------------------------------------------------------------------------
# synthesis for ice40, using icestorm toolchain

SYNTH_MODULES = $(MODULES) top.v
DEVICE = up5k
CONSTRAINTS = icebreaker.pcf
SYNTH_CMD = synth_ice40

# tool binaries
YOSYS = yosys
PNR = nextpnr-ice40
PACKAGE = sg48
PACK = icepack
PROG = iceprog

# synthesize verilog code, produce a json file containing the resulting netlist
$(DESIGN_NAME).json: $(SYNTH_MODULES)
	$(YOSYS) -p "$(SYNTH_CMD) -json $(DESIGN_NAME).json -top top" $(SYNTH_MODULES)

.phony: draw
draw: $(SYNTH_MODULES)
	$(YOSYS) -p "$(SYNTH_CMD) -top top; show top" $(SYNTH_MODULES)

# place and route the design, producing a text file representing the fpga configuration
%.asc: $(CONSTRAINTS) %.json
	$(PNR) --$(DEVICE) \
		$(if $(PACKAGE),--package $(PACKAGE)) \
	--json $(filter-out $<,$^) \
	--pcf $< \
	--asc $@ \
	--timing-allow-fail \
	--freq 114.544 \
	$(if $(PNR_SEED),--seed $(PNR_SEED))

# convert the ascii output file from arachne-pnr into
# a binary bitstream for upload to SPI flash
$(DESIGN_NAME).bin: $(DESIGN_NAME).asc
	$(PACK) $(DESIGN_NAME).asc $(DESIGN_NAME).bin

#--------------------------------------------------------------------------------
.phony: prog clean lint

# upload binary bitstream to the hardware
prog: $(DESIGN_NAME).bin
	$(PROG) $(DESIGN_NAME).bin

clean:
	rm -fv $(DESIGN_NAME).bin $(DESIGN_NAME).asc $(DESIGN_NAME).json $(VVP_TARGET) dump.vcd

# TODO: add ice40 cells to verilator path somehow
lint:
	verilator --lint-only -Wall $(SYNTH_MODULES)
