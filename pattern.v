`default_nettype none
`timescale 1ns/1ps

module pattern (
    
  input [9:0] pixel_x,
  input [9:0] pixel_y,

  output luma,
  output [4:0] color,
  output sat
);

  assign color = pixel_x[8:4];
  // assign luma = pixel_x[4] || pixel_y[5];
  assign luma = 1'b1;
  // assign sat = pixel_y[5];
  assign sat = 1'b1;

endmodule

