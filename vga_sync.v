`default_nettype none
`timescale 1ns/1ps

module vga_sync (
    input wire clk, reset,
    output wire hsync, vsync, video_on, p_tick, burst,
    output wire [9:0] pixel_x, pixel_y
);

    // TIMING (assumes base clock rate of 13.5 MHz)
    // 640 X 480 display resolution (adds up to 800 x 525 total lines)
    // horizontal
    // james note: messing with these to try to get ntsc compatible timing
    // therefore, the numbers will no longer add up to 800 (it ends up being
    // 857 clocks total to get to the specified 15k hsync rate).
    localparam HD = 780;    // display area
    localparam HF = 89;     // front/left border
    localparam HB = 21;     // back/right border
    localparam HR = 71;     // retrace

    // vertical
    localparam VD = 247;    // display area

    // not clear on the timing for these, i just spitballed some numbers
    // that happen to make the total add up to 262
    localparam VF = 4;     // front/left border
    localparam VB = 5;     // back/right border
    localparam VR = 6;      // retrace

    reg [9:0] h_count_reg, h_count_next;
    reg [9:0] v_count_reg, v_count_next;

    reg v_sync_reg;
    reg h_sync_reg;
    wire v_sync_next;
    wire h_sync_next;

    wire h_end, v_end, pixel_tick;

    always @(posedge clk, posedge reset)
        if (reset)
            begin
                v_count_reg <= 0;
                h_count_reg <= 0;
                v_sync_reg <= 1'b0;
                h_sync_reg <= 1'b0;
            end
        else
            begin
                v_count_reg <= v_count_next;
                h_count_reg <= h_count_next;
                v_sync_reg <= v_sync_next;
                h_sync_reg <= h_sync_next;
            end

    // this used to be a divided-down clock
    assign pixel_tick = 1'b1;

    // add up the lengths of the different time segments of the h and v syncs,
    // resetting when that total amount of time has elapsed
    assign h_end = (h_count_reg == (HD + HF + HB + HR - 1));
    assign v_end = (v_count_reg == (VD + VF + VB + VR - 1));

    always @*
        if (pixel_tick)
            if (h_end)
                h_count_next = 0;
            else
                h_count_next = h_count_reg + 1;
        else
            h_count_next = h_count_reg;

    always @*
        if (pixel_tick & h_end)
            if (v_end)
                v_count_next = 0;
            else
                v_count_next = v_count_reg + 1;
        else
            v_count_next = v_count_reg;

    assign h_sync_next = (h_count_reg >= (HD + HB) && h_count_reg <= (HD + HB + HR - 1));
    assign v_sync_next = (v_count_reg >= (VD + VB) && v_count_reg <= (VD + VB + VR - 1));

    assign video_on = (h_count_reg< HD) && (v_count_reg < VD);
    assign burst = (h_count_reg >= (HD + HB + HR + 14) && 
                    h_count_reg <= (HD + HB + HR + 56));

    // outputs
    assign hsync = h_sync_reg;
    assign vsync = v_sync_reg;
    assign pixel_x = h_count_reg;
    assign pixel_y = v_count_reg;
    assign p_tick = pixel_tick;

endmodule
